import express from 'express';
import json from 'body-parser';
export const router = express.Router();


//declarar primer ruta por omision


export default {router}


router.get('/', (req,res)=>{
    //parametros
    const params = {
        NRecibo:req.query.NRecibo,
        Nombre:req.query.Nombre,
        Domicilio:req.query.Domicilio,
        Servicio:req.query.Servicio,
        KWC:req.query.KWC
    }
    res.render('index',params);
})

router.post('/', (req,res)=>{
    //parametros
    const params = {
        NRecibo:req.body.NRecibo,
        Nombre:req.body.Nombre,
        Domicilio:req.body.Domicilio,
        Servicio:req.body.Servicio,
        KWC:req.body.KWC
    }
    res.render('index',params);
})